import numpy as np
import re
import string

n = 26

def caesar_cipher(text: str, step: int, isCipher: bool) -> str:
    result = ""
    for char in text:
        if re.match("[a-zA-Z]", char) is not None:
            if char.isupper():
                result += chr((ord(char) + step*(1 - 2*isCipher) - 65) % n + 65)
            else:
                result += chr((ord(char) + step*(1 - 2*isCipher) - 97) % n + 97)
        else: result += char
    return result

def vigenere_cipher(text: str, key: str, isCipher: bool) -> str:
    result = ""
    for char, kchar in zip(text, key):
        if re.match("[a-zA-Z]", char) is not None:
            if char.isupper():
                result += chr((ord(char) + ord(kchar)*(1 - 2*isCipher)) % n + 65)
            else:
                result += chr((ord(char) + ord(kchar)*(1 - 2*isCipher)) % n + 97)
        else: result += char
    return result   

if __name__ == "__main__":
    step = 4
    text = ""
    key = ""
    with open("./text.txt", 'r') as file_mat:
        for line in file_mat.readlines():
            text += line.strip()
    with open("./key.txt", 'r') as file_mat:
        key = file_mat.readline()
    key = key*(len(text) // len(key) + 1)
    key = key[:len(text)]

    print(f"Step:{step}\nText: {text}\nKey:{key}\n")
    text = caesar_cipher(text, step, False)
    print(f"Encrypted with caesar: {text}\n")
    text = caesar_cipher(text, step, True)
    print(f"Decrypted: {text}\n")
    
    text = vigenere_cipher(text, key, False)
    print(f"Encrypted with vigenere: {text}\n")
    text = vigenere_cipher(text, key, True)
    print(f"Decrypted: {text}")